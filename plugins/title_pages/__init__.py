# -*- coding: utf-8 -*- #

from __future__ import unicode_literals

import logging
import os.path
import re
import pprint

from pelican import contents, signals

logger = logging.getLogger(__name__)


def split_off_title_pages(sender, *rest):
    '''Split off title pages from list of all pages.

    A title page has a path matching one of the regexes in the setting
    TITLE_PAGE_PATH_REGEXES.

    Title pages are removed from the global 'PAGES' list and put into a new
    TITLE_PAGES - dictionary.

    '''
    logger.debug('invoking plugin title_pages')
    title_page_regexes = sender.settings.get('TITLE_PAGE_PATH_REGEXES', {})
    pages = list(sender.context['PAGES'])  # copy of PAGES to iterate over
    title_pages = {
        'index': None,
        'authors': {},
        'categories': {},
        'tags': {},
    }

    for page in pages:
        src = os.path.splitext(page.source_path)[0]
        for typ, regex in title_page_regexes.items():
            match = re.search(regex, src)
            if match:
                sender.context['PAGES'].remove(page)
                groupdict = match.groupdict()
                if 'slug' in groupdict:
                    slug = groupdict['slug']
                    title_pages[typ][slug] = page
                else:
                    if typ == 'index':
                        title_pages[typ] = page
                    else:
                        tpl = ('Could not find slug for title page "%s". '
                               'Does the "%s"-regex specify (?<slug>...)?')
                        raise RuntimeError(tpl % (src, typ))
                break

    sender.context['TITLE_PAGES'] = title_pages
    logger.debug('plugin title_pages is done.')
    logger.debug(pprint.pformat(title_pages))

def register():
    '''Connect the plugin to page_generator_finalized.'''
    signals.page_generator_finalized.connect(split_off_title_pages)
