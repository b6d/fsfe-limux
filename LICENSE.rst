=============================
FSFE-Limux: Lizenzinformation
=============================

Lizenzinformation Limux-Infosite

 * Vorschlag für Pelican_-Theme: `BSD 3-Clause License`_ und kompatible (siehe
   ``/themes``-Verzeichnis)

 * Vorschlag für Textinhalt: `CC BY-SA 4.0`_ (Ist das auch für mögliche Interviews
   und Beiträge anderer sinnvoll?)

 * Photos sind zum Teil Public Domain (von Unsplash_) und zum Teil von
   Shutterstock_ gekauft.

.. _BSD 3-Clause License: http://opensource.org/licenses/BSD-3-Clause
.. _CC BY-SA 4.0: http://creativecommons.org/licenses/by-sa/4.0/
.. _Pelican: http://getpelican.com
.. _Shutterstock: http://www.shutterstock.com
.. _unsplash: http://unsplash.com
