==========
fsfe-limux
==========

Infoseite der FSFE-Fellowshipgruppe München zum LiMux_-Projekt. **[Draft!]**

*fsfe-limux* ist ein Pelican_-Projekt mit zugehörigem Thema.

Pelican ist ein in Python geschriebener Statische-Seiten-Generator, der aus
Eingabedateien im reStructuredText_-Format (``rst``) einfaches HTML erzeugt,
welches problemlos überall gehostet werden kann.


Installation
============

Getestet unter Debian wheezy, sollte in anderen Distributionen analog laufen
(``s/apt-get/<OS_PKG_MANAGER>/``).

Helper-Skripte für Python virtualenv_ installieren (Erlaubt isolierte
Instalation von Python-Paketen im eigenen Homeverzeichnis)::

  $ sudo apt-get install virtualenvwrapper

*Optional:* Buildsystem und Dev-Pakete installieren (beschleunigt das Bauen der
Seite)::

 $ sudo apt-get install build-essential python-dev libpcre3-dev

virtualenv erstellen und für die aktuelle Sitzung aktivieren (Superuser zu sein
ist ab hier nicht mehr nötig)::

 $ mkvirtualenv limux-venv

Das virtualenv wurde hier ``limux-venv`` genannt. Es kann mit ``deactivate``
jederzeit deaktiviert werden. Reaktivieren per ``workon limux-venv``.

Nun können die nötigen Python-Pakete installiert werden::

 $ pip install pelican typogrify

Falls noch nicht geschehen: Repository klonen (evtl. Repo-URL anpassen) und ins
Projektverzeichnis wechseln::

 $ git clone https://bitbucket.org/b6d/fsfe-limux
 $ cd fsfe-limux

Statische Seiten bauen, lokal ausliefern::

 $ make html
 $ make serve

... und ansehen::

 $ x-www-browser localhost:8000

(Ausgabe getestet mit aktuellem Firefox, sollte mit anderen aktuellen Browsern
hoffentlich auch funktionieren.)


Beitragen
=========

Pelican kann Änderungen an den Eingabedateien automatisch erkennen und die
Seite sofort neu bauen. Zum Entwickeln sehr praktisch::

  $ make devserver

Stoppen mit::

  $ make stopserver

Pull-Requests an: https://bitbucket.org/b6d/fsfe-limux (oder anderes
Projekt-Repo vorschlagen).

Werde versuchen, mich an einen vereinfachten Gitflow_-Workflow zu halten -
Entwicklung also im ``develop``-Zweig, ``master`` sollte nach dem ersten
Release immer die Live-Seite widerspiegeln.


Zum Inhalt beitragen
--------------------

Pelican-Doku_ lesen, ``pelicanconf.py`` ansehen, Inhalte in ``/content``
anpassen.

Vorschlag für Struktur dort (wie alles hier offen zur Diskussion).

``/content/Blog``
    Kategorie für aktuelle Beiträge, "Blog" in Ermangelung eines besseren
    Namens (Vorschläge?). Sollten sich öfter mal ändern. Evtl.  auch
    allgemeinere Dinge wie "Was ist freie Software", "Linux auf dem Vormarsch",
    ...

``/content/Gastbeiträge``
    Der Name ist Programm -- Kategorie für Gastbeiträge.  Wer kennt
    jemanden, der beitragen möchte?

``/content/Presse``
    Kategorie für Pressemeldungen.

``/content/pages``
    Pelican unterscheidet zwischen Artikeln (mit Datum, kategorisiert, liegen
    in oben genannten Verzeichnissen) und Seiten (ohne Datum, liegen in
    ``pages``). Seiten werden im Menü unten angezeigt.  Wichtigere Seiten
    können das Metadatum ``in_header_menu`` setzen.

``/content/title_pages``
    Vom Plugin ``title_pages`` benötigt (vgl. ``pelicanconf.py``) - Seiten in
    diesem Verzeichnisbaum können Artikellisten Vorangestellt werden. Die
    "Home"-Seite liegt ebenfalls dort (``index.rst``).

``/content/images``
    Bilder sind *wichtig*. Habe einige auf Shutterstock_ gekauft, andere von
    Unsplash_ (Public Domain) heruntergeladen. Beiträge willkommen. Wäre
    vielleicht auch kein Fehler, allen Autoren ein Gesicht zu geben.

Immer wieder wichtig:

 * Stimmen die genannten Fakten?
 * Ist alles rechtlich ok? (Impressum? Text- und Bildlizenzen? ...)


Zum Webdesign beitragen
-----------------------

Das Pelican-Thema liegt im Verzeichnis ``themes/fsfe`` und ist dort separat
dokumentiert.

Cross-Browser-Tests und -Fixes sind jederzeit willkommen (insbesondere auch für
Microsoft- und Apple-Browser).

Änderungen am Design bitte vorher absprechen. Bei Anpassungen auch an
Mobilgeräte denken.

.. _Gitflow: https://www.atlassian.com/git/workflows#!workflow-gitflow
.. _LiMux: http://www.muenchen.de/rathaus/Stadtverwaltung/Direktorium/LiMux.html
.. _Pelican: http://getpelican.com
.. _Pelican-Doku: http://docs.getpelican.com
.. _reStructuredText: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
.. _Shutterstock: http://www.shutterstock.com
.. _Unsplash: http://unsplash.com
.. _virtualenv: http://virtualenv.org
