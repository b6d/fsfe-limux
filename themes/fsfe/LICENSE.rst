=======
License
=======

This Pelican_ theme as a whole is copyrighted by various authors and is
licensed under the terms of the `BSD 3-Clause License`.


Third-party components incorporated in this theme
=================================================

Various third-party components under compatible licenses are included as part
of this theme.


Pure
----

This theme incorporates and extends the Pure_ CSS library, which is copyright
(c) Yahoo! Inc. and is licensed under the `BSD 3-Clause License`_.

Pure itself builds on the normalize.css_ library which is copyright (c) Nicolas
Gallagher and Jonathan Neal and is licensed under the `MIT License`_.


Font Awesome
------------

This theme incorporates parts of the `Font Awesome`_ project by Dave Gandy,
namely the Font Awesome font which is Licensed under the `SIL Open Font
License`_ (OFL-1.1) and the Font Awesome CSS files licensed under the `MIT
License`.


.. _BSD 3-Clause License: http://opensource.org/licenses/BSD-3-Clause
.. _Font Awesome: http://fontawesome.io
.. _MIT License: http://opensource.org/licenses/MIT
.. _normalize.css: https://http://necolas.github.io/normalize.css
.. _Pelican: http://getpelican.com
.. _Pure: http://purecss.io
.. _SIL Open Font License: http://opensource.org/licenses/OFL-1.1
