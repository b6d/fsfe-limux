==================
FSFE Pelican Theme
==================

A modern Pelican Theme using Pure_.

Utilizes Pelican plugin ``title_pages``, also works without.


Regenerating CSS files
======================

The CSS files in this theme are generated from Sass_ templates in the ``scss/``
directory. These can be recompiled using (for example) pyScss_.

To do so, make sure you have pyScss installed::

 $ pip install pyscss

To generate a CSS file from its SCSS source::

 $ make css

Optionally, for continuous regeneration of all SCSS on change, install watchdog
as well::

 $ pip install watchdog

and start pycss the following way::

 $ make regenerate

Exit pyscss with ``Ctrl+C``.


.. _Pure: http://purecss.io
.. _pyScss: https://pypi.python.org/pypi/pyScss/
.. _Sass: http://sass-lang.com
.. _watchdog: https://pypi.python.org/pypi/watchdog/
