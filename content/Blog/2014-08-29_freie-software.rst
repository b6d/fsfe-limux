==============
Freie Software
==============

:tags: freie software
:featured: featured
:author: FSFE-Fellowship München

:splash_image: images/splash/freedom.jpg
:splash_title: Mehr als nur gratis
:splash_text: Was Freiheit wirklich bedeutet

"Pelican considers \"articles" to be chronological content -- such as posts on a
blog, and thus associated with a date ...

The idea behind "pages" is that they are usually not temporal in nature and are
used for content that does not change very often (e.g., “About” or “Contact”
pages).


Where to find plugins
=====================

We maintain a separate repository of plugins for people to share and use.
Please visit the pelican-plugins repository for a list of available plugins.

Please note that while we do our best to review and maintain these plugins,
they are submitted by the Pelican community and thus may have varying levels of
support and interoperability.


How to create plugins
=====================

Plugins are based on the concept of signals. Pelican sends signals, and plugins
subscribe to those signals. The list of signals are defined in a subsequent
section.

The only rule to follow for plugins is to define a register callable, in which
you map the signals to your plugin logic. Let’s take a simple example:
