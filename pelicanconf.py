#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'FSFE-Fellowship München'
SITENAME = 'LiMux Info'
SITEURL = ''
RELATIVE_URLS = True


### Lokalisierung #############################################################

LOCALE='de_DE.UTF-8'
TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'de'
DEFAULT_DATE_FORMAT = '%a, %d. %B %Y'


### Feeds und Links ###########################################################

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

# (Momentan) nicht vom Thema genutzt
# Blogroll
#LINKS = [
#    ('Pelican', 'http://getpelican.com/'),
#    ('Python', 'http://python.org/'),
#    ('Jinja2', 'http://jinja.pocoo.org/'),
#]
#
# Social widget
#SOCIAL = [
#    ('You can add links in your config file', '#'),
#    ('Another social link', '#'),
#]


### Build #####################################################################

DELETE_OUTPUT_DIRECTORY = True
TYPOGRIFY = True  # requires pip install typogrify
DEFAULT_PAGINATION = False
SLUGIFY_SOURCE = 'basename'
DIRECT_TEMPLATES = [
    'index',
    'categories',
    'authors',
    'tags',
]


### URLs und Pfade ############################################################

# Input-Pfade
PATH = 'content'
STATIC_PATHS = ['images']
PAGE_PATHS = ['pages', 'title_pages']

# Metadaten aus Input-Dateinamen ermitteln
FILENAME_METADATA = '(?P<date>\d{4}-\d{2}-\d{2})_(?P<slug>.*)'

# Artikel-URLs und Pfade
ARTICLE_URL = 'articles/{date:%Y}/{date:%m}/{date:%d}/{slug}/'
ARTICLE_SAVE_AS = ARTICLE_URL + 'index.html'


### Plugins ###################################################################

PLUGIN_PATHS = ['plugins']
PLUGINS = ['title_pages']

# Plugin "title_pages": Reguläre Ausdrücke zum Ermitteln von Titelseiten
TITLE_PAGE_PATH_REGEXES = {
    'index': r'/title_pages/index$',
    'authors': r'/title_pages/authors/(?P<slug>[^/]+)$',
    'categories': r'/title_pages/categories/(?P<slug>[^/]+)$',
    'tags': r'/title_pages/tags/(?P<slug>[^/]+)$',
}


### Theme #####################################################################

THEME = 'themes/fsfe'

# Wenn ein Artikel kein anderes Bild definiert hat (metadata :splash_image:),
# dann das folgende verwenden:
FSFE_DEFAULT_SPLASH_IMAGE = 'images/splash/muenchen.jpg';
FSFE_DEFAULT_SPLASH_IMAGE_POSITION = '40% 10px'

# Autoren, von deren Bilder In Artikeln erscheinen
FSFE_AUTHORS = {
    'FSFE-Fellowship München': {
        'image': 'images/authors/default.jpg',
        'email': 'muenchen@fsfe.org',  # E-Mail-Adresse anpassen!!!
    },
    # evtl. hier individuelle Autoren (Gastautoren?) anführen
    #'Bernhard Weitzhofer': {
    #    'image': 'images/authors/bernhard_w.jpg',
    #    'email': 'Bernhard.Weitzhofer@fsfe.org',
    #},
}
